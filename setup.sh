#!/bin/bash

command -v git >/dev/null 2>&1 || { echo >&2 "git is required, but does not exist on the system.. aborting"; exit 1; }
command -v curl >/dev/null 2>&1 || { echo >&2 "For Vundle to function, curl is required. Install before running `VundleInstall` from vim"; }

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd)"

git clone https://github.com/VundleVim/Vundle.vim.git $HOME/.vim/bundle/Vundle.vim
git clone https://github.com/chriskempson/base16-shell.git $HOME/.config/base16-shell

mkdir -p $HOME/.vimruntime/backup
mkdir -p $HOME/.vimruntime/temp
mkdir -p $HOME/.vimruntime/undodir
mkdir -p $HOME/.vim/plugin

cp $DIR/.bashrc $HOME/
cp $DIR/.vimrc $HOME/
cp $DIR/.tmux.conf $HOME/

source $HOME/.bashrc
