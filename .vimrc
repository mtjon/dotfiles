set nocompatible " be iMproved, required
filetype off                " Required


" Plugins
" -------

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'tpope/vim-surround'
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'tpope/vim-abolish'
Plugin 'tpope/vim-unimpaired'
Plugin 'dhruvasagar/vim-table-mode'
Plugin 'hashivim/vim-terraform'
Plugin 'godlygeek/tabular'

call vundle#end()           " required
filetype plugin indent on   " required

" Settings
" --------

syntax on
set hidden " Allow for more than a single buffer
set autoindent " Carry over indents to newline
set smartindent " Adjust indentation based on brackets, etc.
set incsearch " Instant search while typing
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set title
set showmatch       " Show matching brackets
set cursorline " Highlight current line

" Leave only footprints
set backup
set backupdir=~/.vimruntime/backup
set directory=~/.vimruntime/temp

" Undo across filecloses
set undofile
set undodir=~/.vimruntime/undodir
set undolevels=1000
set undoreload=10000

set wildmenu " use wildmenu
set wildmode=list:longest,full

" autosave on lost focus
au FocusLost * silent ! wa

" finding files
set path+=**

" line numbers
set number relativenumber

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter * set norelativenumber
augroup END

" diff colours
if &diff
    colorscheme github
else
    colorscheme default
endif

" Mappings
" --------

" Visual line movement, as opposed to line numbers.
:map j gj
:map k gk

" Easier to engage ex-mode
nnoremap ; : 

" Language-specific settings

" YAML
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
